import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import Login from'./screens/Login';
import HomeScreen from './screens/Dashboard';
import SignUp from'./screens/UserSignUp';
export default class App extends React.Component {
  constructor(props) {
    super(props);
  this.state = {
   display: <Login updateLogin={this.openHomeScreen} signUp={this.openSignUp} />
  };
}
logout = () =>{
  console.log("Showing Login Screen");
  this.setState({display: <Login updateLogin={this.openHomeScreen} signUp={this.openSignUp}/>})
}
  openHomeScreen = () =>{
    console.log("Login Succes , Opening HomeScreen");
    this.setState({display: <HomeScreen logout={this.logout}/>})
  }
  openSignUp = () =>{
    console.log("Login Succes , Opening Home Screen");
    this.setState({display: <SignUp logout={this.logout}/>})
  }
  render() {
    
      return (<PaperProvider>
          {this.state.display}
          </PaperProvider>
      );
    }
  }
  