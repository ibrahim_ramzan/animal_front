import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text } from 'react-native';
import { Button,DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';
import { IconButton, Colors,Chip } from 'react-native-paper';
export default class App extends React.Component {
  constructor(props) {
    super(props);
 this.state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first'
  };
}

  logout = () =>
  {
    console.log("presses log out");
  }
  render() {
    const { active } = this.state;
      return (
        <PaperProvider theme={theme} style={{width:'80%'}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Animal Care" style={{marginLeft:2}}
        />
       <Appbar.Action icon="settings-power" onPress={() => this.props.logout()} />
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Chip style={{alignItems:'center',borderColor:'#b33939',borderRadius:0,borderWidth:3,fontSize:20,color:'#b33939'}} Type="outlined"><Text style={{fontSize:20,color:'#b33939',fontWeight: 'bold'}}>Dashboard</Text></Chip>

        <View style={styles.container}>
              <View style={styles.buttonContainer}>
              <Button icon="favorite-border" style={{marginRight:8,height:100,justifyContent:'center',fontSize:45,marginTop:10}} mode="contained" onPress={() => console.log('Pressed')}>
              
               Donate
  </Button>
              </View>
              <View style={styles.buttonContainer}>
              <Button icon="local-pharmacy" mode="contained" onPress={() => console.log('Pressed')} style={{height:100,justifyContent:'center',fontSize:45,marginTop:10}}>
    Nearest Vets
  </Button>
              </View>
            </View>


            <View style={styles.container}>
              <View style={styles.buttonContainer}>
              <Button icon="add-a-photo" style={{marginRight:8,height:100,justifyContent:'center',fontSize:45,marginTop:10}} mode="contained" onPress={() => console.log('Pressed')}>
              
               Pet Finder
  </Button>
              </View>
              <View style={styles.buttonContainer}>
              <Button icon="attach-money" mode="contained" onPress={() => console.log('Pressed')} style={{height:100,justifyContent:'center',fontSize:45,marginTop:10}}>
    Buy,Sell
  </Button>
              </View>
            </View>


            <Button icon="whatshot" mode="contained" style={{height:60,alignItems:'center',justifyContent:'center',marginTop:132}} onPress={() => console.log('Pressed')}>
    Animal Emergency
  </Button>
  
  
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#b33939',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    container: {
        //flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flex: 1,
    },
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:0,
     // height: '100%',
      width: '100%',
     // alignItems: 'left',
      justifyContent: 'center',
      elevation: 0,
    },
  });
