import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text,ScrollView,Dimensions } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first'
  };

  render() {
    const { active } = this.state;
    const screenHeight = Dimensions.get('window').height;
      return (
        <PaperProvider theme={theme} style={{width:'80%',height:screenHeight}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Animal Care" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Title style={{color:"#b33939"}}>Create New Account </Title>
        <ScrollView 
    behaviour = "height"
    keyboardVerticalOffset = {64}
    style= {{ marginTop: '5%',width:"100%",height:"100%",padding:5}}
  >
        
        <TextInput style={styles.text}
        label='Company Name'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
        <TextInput style={styles.text}
        label='Email Address'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
     <TextInput style={styles.text}
        label='City'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
      <TextInput style={styles.text}
        label='Phone No.'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
       <TextInput style={styles.text}
        label='Address'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
       <TextInput style={styles.text}
        label='Timing'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
       <TextInput style={styles.text}
        label='ISO'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
       <TextInput style={styles.text}
        label='Website (Optional)'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
      


  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#b33939" onPress={() => console.log('Login Pressed')}>
      
    Create Account
  </Button>

  <Button icon="backspace" mode="contained" style={styles.button} color="#b33939" onPress={() => console.log('Login Pressed')}>
      
    Cancel
  </Button>
 <View style={{padding:50}}></View>

  </ScrollView>
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#b33939',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      paddingBottom:25,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
