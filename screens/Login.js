import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  this.state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    password :'',
   
  };
}
trySignup = () =>
{
  console.log("opening signup");
  this.props.signUp();
}
tryLogin  = () =>{

fetch("http://api.savetails.com:8080/user/login", {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    email: this.state.user,
    password: this.state.password,
  }),
})

  .then((response) => response.json())
  .then((responseJson) => {
      console.log(responseJson, 'res JSON');
      //alert(responseJson.message);
      console.log(this.state.user);
      console.log(this.state.password);
      if(responseJson.message == "Auth successful")
      {
        //alert("Logged In Successfully");
        this.props.updateLogin();
      }
      else{
        alert("Invalid Username/Password");
      }
  })
  .catch((error) => {
      console.error(error);
  });
  }
  render() {
    const { active } = this.state;
      return (
        <PaperProvider theme={theme} style={{width:'80%'}}>
        <Appbar.Header>
        <Appbar.Content
          title="Animal Care"
        />
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        <Avatar.Icon size={70} icon="beach-access" />
        <Title style={{color:"#b33939"}}>Login- Animal Care</Title>

      <TextInput style={styles.text}
        label='Email'
        Type="outlined"
        value={this.state.user}
        onChangeText={user => this.setState({ user })}
      />
    
      <TextInput style={styles.text}
        label='Password'
        Type="outlined"
        value={this.state.password}
        onChangeText={password => this.setState({ password })}
      />
      <Text style={{color:"#b33939",margin:10,}}>Forgot Password ?</Text>
      <Button icon="navigate-next" mode="contained" style={styles.button} color="#b33939" onPress={this.tryLogin}>
      
    Login
  </Button>

  <Button icon="add-circle-outline" mode="contained" style={styles.button} color="#b33939" onPress={this.trySignup}>
      
    Create New Account
  </Button>
  
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#b33939',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:40,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
