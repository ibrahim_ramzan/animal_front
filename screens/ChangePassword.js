import React from 'react';
import { Platform, StatusBar, StyleSheet, View,Text } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { TextInput,Title,Button,Surface } from 'react-native-paper';
import { Avatar,Divider,Drawer } from 'react-native-paper';

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
    text:'',
    user:'',
    active:'first'
  };

  render() {
    const { active } = this.state;
      return (
        <PaperProvider theme={theme} style={{width:'80%'}}>
        <Appbar.Header>
        <Avatar.Icon size={50} style={{marginRight:2}} icon="beach-access" />
        <Appbar.Content
          title="Animal Care" style={{marginLeft:2}}
        />
       
      </Appbar.Header>
        
      
        <Surface style={styles.surface}>
        
        <Title style={{color:"#b33939"}}>Change Password </Title>

        <TextInput style={styles.text}
        label='Old Password'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
      <TextInput style={styles.text}
        label='New Password'
        Type="outlined"
        value={this.state.user}
        onChangeText={user => this.setState({ user })}
      />
    
      <TextInput style={styles.text}
        label='Re-Enter New Password'
        Type="outlined"
        value={this.state.text}
        onChangeText={text => this.setState({ text })}
      />
      


  <Button icon="create" mode="contained" style={styles.button} color="#b33939" onPress={() => console.log('Login Pressed')}>
      
    Change Password
  </Button>

  
  
  </Surface>
        </PaperProvider>
      );
    }
  }
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#b33939',
      accent: '#ff5252',
    },
  };
  const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    text:{
      width:'95%',
      borderRadius:0,
      backgroundColor:"white",
      marginBottom:10
    }
    ,
    button:{
      color:'white',
      marginTop:10,
      width:'95%'
    },
    surface: {
      padding: 8,
      marginTop:10,
      //height: 80,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 0,
    },
  });
